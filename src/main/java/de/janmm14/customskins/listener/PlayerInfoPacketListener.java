package de.janmm14.customskins.listener;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import de.janmm14.customskins.CustomSkins;
import de.janmm14.customskins.data.Skin;

import com.comphenix.packetwrapper.WrapperPlayServerPlayerInfo;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.injector.GamePhase;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedSignedProperty;
import com.google.common.base.Supplier;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Table;
import org.jetbrains.annotations.NotNull;
import lombok.NonNull;

public class PlayerInfoPacketListener extends PacketAdapter implements Listener {

	@NonNull
	private final CustomSkins plugin;
	public static final Supplier<List<WrappedSignedProperty>> ARRAY_LIST_SUPPLIER = new Supplier<List<WrappedSignedProperty>>() {
		@Override
		@NotNull
		public List<WrappedSignedProperty> get() {
			return Lists.newArrayList();
		}
	};

	public PlayerInfoPacketListener(@NonNull CustomSkins plugin) {
		super(new AdapterParameteters()
			.plugin(plugin)
			.gamePhase(GamePhase.BOTH)
			.serverSide()
			.optionAsync()
			.listenerPriority(ListenerPriority.HIGHEST)
			.types(PacketType.Play.Server.PLAYER_INFO));
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	/**
	 * shownTo, shown -> display name
	 */
	@NonNull
	private final Table<UUID, UUID, WrappedChatComponent> displayNameCache = HashBasedTable.create();
	@NonNull
	private final ReentrantLock displayNameCacheLock = new ReentrantLock();

	@NonNull
	private final Map<UUID, WrappedSignedProperty> originSkinInfo = new HashMap<>();

	@Override
	public void onPacketSending(PacketEvent event) {
		if (event.getPacketType() != PacketType.Play.Server.PLAYER_INFO) {
			return;
		}
		final WrapperPlayServerPlayerInfo packet = new WrapperPlayServerPlayerInfo(event.getPacket());
		final List<PlayerInfoData> packetData = packet.getData();
		if (packet.getAction() == EnumWrappers.PlayerInfoAction.UPDATE_DISPLAY_NAME) {
			if (packetData == null) {
				plugin.getLogger().warning("packetData is null, could not change any skins");
				return;
			}
			for (PlayerInfoData pd : packetData) {
				if (pd.getDisplayName() == null) {
					displayNameCacheLock.lock();
					displayNameCache.remove(event.getPacket(), pd.getProfile().getUUID());
					displayNameCacheLock.unlock();
				} else {
					displayNameCacheLock.lock();
					displayNameCache.put(event.getPlayer().getUniqueId(), pd.getProfile().getUUID(), pd.getDisplayName());
					displayNameCacheLock.unlock();
				}
			}
			return;
		} else if (packet.getAction() != EnumWrappers.PlayerInfoAction.ADD_PLAYER) {
			return;
		}
		if (packetData == null) {
			plugin.getLogger().warning("packetData is null, could not change any skins");
			return;
		}
		try {
			List<PlayerInfoData> newData = new ArrayList<>(packetData.size());
			for (PlayerInfoData playerInfoData : packetData) {
				WrappedGameProfile profile = playerInfoData.getProfile();
				if (profile == null) {
					plugin.getLogger().warning("profile is null, could not change any skins");
					return;
				}

				// iterate through properties safely (currently only textures property is there)
				// map for property replacements
				final Multimap<String, WrappedSignedProperty> profileProperties = profile.getProperties();

				final Multimap<String, WrappedSignedProperty> newProfileProperties = Multimaps.newListMultimap(Maps.<String, Collection<WrappedSignedProperty>>newHashMapWithExpectedSize(profileProperties.size()), ARRAY_LIST_SUPPLIER);

				for (Map.Entry<String, WrappedSignedProperty> entry : profileProperties.entries()) {
					if (entry == null) {
						continue;
					}
					if (entry.getKey().equalsIgnoreCase("textures")) {
						String skinName = plugin.getData().getSkinNameIdByUUID(profile.getUUID());
						// if skin is changed
						if (skinName != null && !skinName.isEmpty()) {
							Skin skin = plugin.getData().getCachedSkin(skinName);
							//ensure the skin exists
							if (skin != null) {
								//put value in replacement map
								newProfileProperties.put(entry.getKey(), new WrappedSignedProperty(
									entry.getValue().getName(),
									skin.getData(),
									skin.getSignature()));
								continue;
							}
						}
					}
					// if skin is ot changed, put old values in map
					newProfileProperties.put(entry.getKey(), entry.getValue());
				}
				//if no texture data is where, create some and apply changed skin directly if happend
				if (newProfileProperties.isEmpty()) {
					String skinName = plugin.getData().getSkinNameIdByUUID(profile.getUUID());
					if (skinName != null && !skinName.isEmpty()) {
						Skin skin = plugin.getData().getCachedSkin(skinName);
						if (skin != null) {
							newProfileProperties.put("textures", new WrappedSignedProperty("textures", skin.getData(), skin.getSignature()));
						} else {
							// fill map up with old data if available & no new data is available
							if (!profileProperties.isEmpty()) {
								newProfileProperties.putAll(profileProperties);
							}
						}
					} else {
						// fill map up with old data if available & no new data is available
						if (!profileProperties.isEmpty()) {
							newProfileProperties.putAll(profileProperties);
						}
					}
				}
				// apply changes from our map
				profileProperties.clear();
				profileProperties.putAll(newProfileProperties);

				// ensure we don't change the display name of the specific user
				WrappedChatComponent displayName = playerInfoData.getDisplayName();
				if (displayName == null) {
					//displayName = WrappedChatComponent.fromText(profile.getName()); //protocollib seems to require this optinal field
				} else {
					displayNameCacheLock.lock();
					displayNameCache.put(event.getPlayer().getUniqueId(), profile.getUUID(), displayName);
					displayNameCacheLock.unlock();
				}
				// add player info data to new data list
				newData.add(new PlayerInfoData(profile, playerInfoData.getPing(), playerInfoData.getGameMode(), displayName));
			}

			try {
				packet.setData(newData);
			} catch (RuntimeException e) {
				// always set the packet if any changes worked
				event.setPacket(packet.getHandle());
				//throw exception again so the detailed error message will appear
				throw e;
			}
			event.setPacket(packet.getHandle());
		} catch (Throwable t) {
			//detailed and fast error mesage (StringBuilder is used)
			StringBuilder sb = new StringBuilder();
			boolean first = true;
			for (PlayerInfoData playerInfoData : packetData) {
				if (first)
					first = false;
				else
					sb.append(", ");
				sb.append(playerInfoData.getProfile().getName());
			}
			plugin.getLogger().severe("Could not transform player list packet for player " + event.getPlayer().getName() + " about player's " + sb.toString());
			t.printStackTrace();
		}
	}

	public void updatePlayerSkin(@NonNull final Player target) {
		// first remove player's of tablist so we can set skin data again
		try {
			WrapperPlayServerPlayerInfo packet = new WrapperPlayServerPlayerInfo();
			packet.setAction(EnumWrappers.PlayerInfoAction.REMOVE_PLAYER);
			WrappedGameProfile profile = new WrappedGameProfile(target.getUniqueId(), null);
			packet.setData(Lists.newArrayList(new PlayerInfoData(profile, 0, EnumWrappers.NativeGameMode.SURVIVAL, WrappedChatComponent.fromText(""))));
			for (Player plr : plugin.getServer().getOnlinePlayers()) {
				if (plr.canSee(target)) {
					packet.sendPacket(plr);
				}
			}
		} catch (Throwable t) {
			plugin.getLogger().severe("Could not update player skin for player " + target.getName() + ": Sending remove old skin failed!");
			t.printStackTrace();
		}
		// when create new playerlistpacket containing all the data of the player
		try {
			WrapperPlayServerPlayerInfo basePacket = new WrapperPlayServerPlayerInfo();
			basePacket.setAction(EnumWrappers.PlayerInfoAction.ADD_PLAYER);
			WrappedGameProfile profile = new WrappedGameProfile(target.getUniqueId(), target.getName());
			final EnumWrappers.NativeGameMode nativeGameMode = EnumWrappers.NativeGameMode.fromBukkit(target.getGameMode());

			// get ping via reflection
			int ping;
			Class<?> craftPlayer = target.getClass();
			try {
				Method getHandle = craftPlayer.getMethod("getHandle", (Class[]) null);
				Object entityPlayer = getHandle.invoke(target);
				Field pingField = entityPlayer.getClass().getField("ping");
				ping = (int) pingField.get(entityPlayer);
			} catch (ReflectiveOperationException e) {
				plugin.getLogger().warning("Could not get ping of player " + target.getName() + ". Error:");
				e.printStackTrace();
				ping = 0;
			}
			// send to all players
			for (Player plr : plugin.getServer().getOnlinePlayers()) {
				// ... except the ones who are hidden from the player
				if (plr.canSee(target)) {
					WrappedChatComponent displayName = null;

					// look whether the display name was changed
					displayNameCacheLock.lock();
					if (displayNameCache.contains(plr, target))
						displayName = displayNameCache.get(plr, target);
					displayNameCacheLock.unlock();

					// we have to set the display name as protocollib does not like it if we leave it out (wiki.vg says the packet allowes leaving out)
					if (displayName == null) {
						//displayName = WrappedChatComponent.fromText(target.getName());
					}

					//create and send the packet
					WrapperPlayServerPlayerInfo packet = new WrapperPlayServerPlayerInfo(basePacket.getHandle());
					packet.setData(Collections.singletonList(new PlayerInfoData(profile, ping, nativeGameMode, displayName)));
					packet.sendPacket(plr); //invoke our own packet handler as this does not set any skin-related data
				}
			}
		} catch (Throwable t) {
			plugin.getLogger().severe("Could not update player skin for player " + target.getName() + ": Sending new skin data failed!");
			t.printStackTrace();
		}
		// TODO test whether we have to hide and show the player for the skin to update
		try {
			for (Player plr : plugin.getServer().getOnlinePlayers()) {
				if (plr.canSee(target)) {
					plr.hidePlayer(target);
					plr.showPlayer(target);
				}
			}
		} catch (IllegalStateException ex) {
			//in case spigot does not like async, just do it sync!
			Bukkit.getScheduler().runTask(plugin, new Runnable() {
				@Override
				public void run() {
					for (Player plr : plugin.getServer().getOnlinePlayers()) {
						if (plr.canSee(target)) {
							plr.hidePlayer(target);
							plr.showPlayer(target);
						}
					}
				}
			});
		}
	}

	private void removeCachedDisplayNames(@NonNull UUID uuid) {
		displayNameCacheLock.lock();
		Map<UUID, WrappedChatComponent> row = new HashMap<>(displayNameCache.row(uuid));
		for (UUID shown : row.keySet()) {
			displayNameCache.remove(uuid, shown);
		}
		Map<UUID, WrappedChatComponent> column = new HashMap<>(displayNameCache.column(uuid));
		for (UUID shownTo : column.keySet()) {
			displayNameCache.remove(shownTo, uuid);
		}
		displayNameCacheLock.unlock();
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerQuit(PlayerQuitEvent event) {
		removeCachedDisplayNames(event.getPlayer().getUniqueId());
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerKick(PlayerKickEvent event) {
		removeCachedDisplayNames(event.getPlayer().getUniqueId());
	}
}
