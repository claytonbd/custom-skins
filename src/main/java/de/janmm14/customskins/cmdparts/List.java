package de.janmm14.customskins.cmdparts;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.janmm14.customskins.CmdPart;
import de.janmm14.customskins.CustomSkins;
import de.janmm14.customskins.data.Data;

import com.google.common.collect.Lists;
import lombok.NonNull;

public class List extends CmdPart {

	public static final BaseComponent[] USAGE = new ComponentBuilder("/customskins list").color(ChatColor.DARK_GRAY).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/customskins list"))
		.append(" - ").color(ChatColor.DARK_GRAY)
		.append("lists all downloaded skins").color(ChatColor.DARK_GRAY).create();

	public List() {
		super("customskins.list", "list");
	}

	@Override
	public void onCommand(@NonNull CommandSender cs, @NonNull String[] restArgs) {
		cs.sendMessage("§6All downloaded custom skins:"); //TODO pages?
		final Data data = CustomSkins.getPlugin().getData();
		int i = 0;
		StringBuilder sb = new StringBuilder();
		for (String skinName : data.getAllSkinNames()) {

				if (i % 2 == 1) {
					sb.append("§6");
				} else {
					sb.append("§e");
				}
				sb.append(skinName);
				i++;
		}
		cs.sendMessage(sb.toString());
	}

	@NonNull
	@Override
	public java.util.List<String> onTabComplete(@NonNull CommandSender cs, @NonNull String[] restArgs) {
		return Lists.newArrayList();
	}

	@Override
	protected void sendUsageToNonPlayer(@NonNull CommandSender cs) {
		cs.sendMessage("§8/customskins list - lists all downloaded skins");
	}

	@Override
	protected void sendUsageToPlayer(@NonNull Player p) {
		p.spigot().sendMessage(USAGE);
	}
}
